/**
 * @file Manager.h
 * @author jmmut
 * @date 2018-05-27.
 */

#ifndef MYTEMPLATEPROJECT_MANAGER_H
#define MYTEMPLATEPROJECT_MANAGER_H

#include "events/Dispatcher.h"
#include "events/EventCallThis.h"
#include "manager_templates/MediumManagerBase.h"
#include "Window2D.h"
#include "MovingPoint.h"

static const double INITIAL_SPEED = 45.0;
static const std::size_t INITIAL_APPEARANCE_RATE = 2;
static const double INITIAL_SIZE = 5.0;

class Manager: public MediumManagerBase {
public:
    Manager(Dispatcher &d, std::shared_ptr<Window> w)
            : MediumManagerBase(d, w), appearanceRate{INITIAL_APPEARANCE_RATE}, initialSpeed{INITIAL_SPEED},
            initialSize{INITIAL_SIZE} {
        restartPoints();
    }

    void restartPoints() {
        points.clear();
        points.push_back(MovingPoint{{0.0, 0.0}, {0.0, initialSpeed}, {initialSize, initialSize}});
        iterations = 0;
    }

protected:
    void onDisplay( Uint32 ms) override;

    void onKeyChange(SDL_KeyboardEvent &event) override;

    Window2D &tryGetWindow();

private:
    std::vector<MovingPoint> points;
    std::size_t iterations = 0;
    std::size_t appearanceRate;
    double initialSpeed;
    double initialSize;

    SDL_Rect getRect(Position position, Position size) const;
    void updatePoints();
    Position pointSizeFunction(const MovingPoint &point);
    std::complex<double> pointSpeedFunction(const MovingPoint &point);
    void drawPoints();
    void createNewPoints();
    void measureDensity();
};


#endif //MYTEMPLATEPROJECT_MANAGER_H
