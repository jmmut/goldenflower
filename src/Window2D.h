/**
 * @file Manager.h
 * @author jmmut
 * @date 2018-05-27.
 */

#ifndef MYTEMPLATEPROJECT_WINDOW_H
#define MYTEMPLATEPROJECT_WINDOW_H

#include <random>
#include <chrono>
#include <complex>
#include <graphics/Window.h>

class Window2D : public WindowRenderer {
public:
    explicit Window2D(Uint32 flags = 0) : WindowRenderer{flags} {}
    ~Window2D() override = default;

    void open() override;
    void drawRect(Uint8 red, Uint8 green, Uint8 blue, Uint8 alpha, SDL_Rect &rect);
    void fillRect(Uint8 red, Uint8 green, Uint8 blue, Uint8 alpha, SDL_Rect &rect);
    void renderClearBlack();
    void setTitle(const std::string &title);

    void resize(Uint32 w, Uint32 h) override;
};
#endif //MYTEMPLATEPROJECT_WINDOW_H
