/**
 * @file Manager.cpp
 * @author jmmut
 * @date 2018-05-27.
 */

#include "Manager.h"
#include "utils/exception/StackTracedException.h"
#include "Window2D.h"

static const double PHI = 1.61803398875;

void Manager::onDisplay(Uint32 ms) {
    auto &window = tryGetWindow();

    auto width = 0, height = 0;
    window.getWindowSize(width, height);
    window.renderClearBlack();

    iterations++;
    createNewPoints();
    updatePoints();
    drawPoints();
    measureDensity();

    window.renderPresent();
}

Window2D &Manager::tryGetWindow() {
    auto ptr = this->window.lock();
    if (ptr == nullptr) {
        throw randomize::utils::exception::StackTracedException("Window unavailable");
    } else {
        return dynamic_cast<Window2D &>(ptr.operator*());
    }
}

void Manager::createNewPoints() {
    if (iterations % appearanceRate == 0) {
        auto previousPointAngle = arg(points.back().speed);
        auto goldenAngle = 2 * M_PI * PHI;
        auto newPointSpeed = std::polar(initialSpeed, previousPointAngle + goldenAngle);
        points.push_back(MovingPoint{{0.0, 0.0}, newPointSpeed, {initialSize,initialSize}});
    }
}

void Manager::updatePoints() {
    for (auto &point : points) {
        auto dt = 1.0;
        point.position += point.speed * dt; // integral of speed: p_1 = v·dt + p_0
        point.speed = pointSpeedFunction(point);
        point.size = pointSizeFunction(point);
    }
}

Position Manager::pointSpeedFunction(const MovingPoint &point) {
    auto directionAngle = std::arg(point.speed);

    auto distance = std::abs(point.position);
    auto inverse = 1.0 / distance;
    auto newVelocity = inverse * initialSpeed;

    //    auto growingRate = 1.0 / appearanceRate;
    //    auto newVelocity = 1.0 / std::sqrt(growingRate * iterations);

    //    auto distance = std::abs(point.position);
    //    auto inverse = 1.0 / distance;
    //    auto density = 0.1;
    //    auto k = appearanceRate / std::pow((M_PI * density), 1.5);
    //    auto newVelocity = inverse * k;

    //    auto distance = std::abs(point.position);
    //    auto inverse = 1.0 / distance;
    //    auto k = 1;
    //    auto newVelocity = inverse * k;

    auto newSpeed = std::polar(newVelocity, directionAngle);

    // doesn't work
    //    auto dt = 0.00001;
    //    auto k = 0.0000000001;
    //    auto velocity = std::abs(point.speed);
    //    auto acceleration = k * std::pow(velocity, 3);
    //    auto newSpeed = point.speed + std::polar(dt * acceleration, directionAngle);

    //    auto dt = 1.0;
    //    auto growingRate = 1.0 / appearanceRate;
    //    auto density = 0.05;
    //    auto k = density * density * M_PI * M_PI / (2 * growingRate);
    //    auto velocity = std::abs(point.speed);
    //    auto acceleration = k * std::pow(velocity, 3.0);
    //    auto newSpeed = point.speed - std::polar(dt * acceleration, directionAngle);

    return newSpeed;
}

Position Manager::pointSizeFunction(const MovingPoint &point) {
    //    auto separation = distance(center, point.position);

    //    auto separation = std::abs(point.position);
    //    auto size = std::log2(separation + 1);
    //    return {size, size};

    return {(initialSize), static_cast<double>(initialSize)};
}

void Manager::drawPoints() {
    auto &window = tryGetWindow();
    auto width = 0, height = 0;
    window.getWindowSize(width, height);
    auto center = Position{width/2.0, height/2.0};
    for (const auto &point : points) {
        auto rect = getRect(center + point.position, point.size);
        window.drawRect(100, 200, 255, 255, rect);
    }
}

SDL_Rect Manager::getRect(Position position, Position size) const {
    SDL_Rect rect{
            //            static_cast<int>(position.first),
            //            static_cast<int>(position.second),
            //            static_cast<int>(size),
            //            static_cast<int>(size)};
            static_cast<int>(position.real()),
            static_cast<int>(position.imag()),
            static_cast<int>(size.real()),
            static_cast<int>(size.imag())};
    return rect;
}

void Manager::measureDensity() {
    if (not points.empty() and iterations % 200 == 0) {
        auto radius = abs(points.front().position);
        auto measuredDensity = points.size() / (radius * radius * M_PI);
        tryGetWindow().setTitle("measuredDensity: " + std::to_string(measuredDensity));
    }
}

void Manager::onKeyChange(SDL_KeyboardEvent &event) {

    if (event.state == SDL_PRESSED) {
        switch (event.keysym.sym) {
        case SDLK_p:
        case SDLK_SPACE:
            if (displayActive) {
                stopOnDisplay();
            } else {
                startOnDisplay();
            }
            break;
        case SDLK_KP_PLUS:
        case SDLK_PLUS:
        case SDLK_f:
            setDisplayFPS(getDisplayFPS() + 1);
            tryGetWindow().setTitle("FPS: " + std::to_string(getDisplayFPS()));
            break;
        case SDLK_KP_MINUS:
        case SDLK_MINUS:
        case SDLK_g:
            setDisplayFPS(std::max(getDisplayFPS() - 1, Uint32(1)));
            tryGetWindow().setTitle("FPS: " + std::to_string(getDisplayFPS()));
            break;
        case SDLK_s:
            initialSpeed++;
            tryGetWindow().setTitle("Increasing speed to " + std::to_string(initialSpeed));
            break;
        case SDLK_x:
            initialSpeed = std::max(initialSpeed - 1, 1.0);
            tryGetWindow().setTitle("Decreasing speed to " + std::to_string(initialSpeed));
            break;
        case SDLK_a:
            appearanceRate = std::max(appearanceRate - 1, 1UL);
            tryGetWindow().setTitle("A new point appears each " + std::to_string(appearanceRate) + " frames");
            break;
        case SDLK_z:
            appearanceRate++;
            tryGetWindow().setTitle("A new point appears each " + std::to_string(appearanceRate) + " frames");
            break;
        case SDLK_d:
            initialSize++;
            tryGetWindow().setTitle("Point size: " + std::to_string(initialSize));
            break;
        case SDLK_c:
            initialSize = std::max(initialSize - 1, 2.0);
            tryGetWindow().setTitle("Point size: " + std::to_string(initialSize));
            break;
        case SDLK_r:
            restartPoints();
            break;
        }
    }
}
