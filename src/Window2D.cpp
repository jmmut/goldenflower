/**
 * @file Window2D.cpp
 * @author jmmut
 * @date 2018-05-27.
 */

#include <algorithm>
#include <complex>
#include "Window2D.h"
#include "utils/exception/StackTracedException.h"
#include "utils/time/TimeRecorder.h"

using randomize::utils::exception::StackTracedException;
using namespace std::string_literals;

void Window2D::open() {
    WindowRenderer::open();
    if (renderer == nullptr) {
        throw StackTracedException("renderer was null. inner message: "s + SDL_GetError());
    }
    SDL_SetRenderTarget(renderer, NULL);
    renderClearBlack();
}

void Window2D::drawRect(Uint8 red, Uint8 green, Uint8 blue, Uint8 alpha, SDL_Rect &rect) {
    SDL_SetRenderDrawColor(renderer, red, green, blue, alpha);
    SDL_RenderDrawRect(renderer, &rect); // only outline
}

void Window2D::fillRect(Uint8 red, Uint8 green, Uint8 blue, Uint8 alpha, SDL_Rect &rect) {
    SDL_SetRenderDrawColor(renderer, red, green, blue, alpha);
    SDL_RenderFillRect(renderer, &rect);
}

void Window2D::renderClearBlack() {
    SDL_SetRenderDrawColor(renderer, 0x00, 0x00, 0x00, 0x00);
    WindowRenderer::renderClear();
}

void Window2D::setTitle(const std::string &title) {
    SDL_SetWindowTitle(window, title.c_str());
}

void Window2D::resize(Uint32 w, Uint32 h) {
    Window::resize(w, h);
    renderClearBlack();
}
