/**
 * @file MovingPoint.h
 * @author jmmut
 * @date 2019-03-24.
 */

#ifndef MYTEMPLATEPROJECT_MOVINGPOINT_H
#define MYTEMPLATEPROJECT_MOVINGPOINT_H


#include <utility>
#include <complex>

using Position = std::complex<double>;

class MovingPoint {
public:
    Position position;
    Position speed;
    Position size;
};


#endif //MYTEMPLATEPROJECT_MOVINGPOINT_H
