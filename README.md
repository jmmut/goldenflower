# Golden flower

Can you generate objects from a single place and keep them organized easily?

[video](https://www.youtube.com/watch?v=eGBTp9fI6F8)

One way to do that is shown by this project. The objects are generated at the center in different (straight) 
directions, forming a circle. By decreasing the speed of each point proportionally to the distance to the center, a 
constant density is achieved in every part of the circle.

Moreover, the direction is chosen by increasing the angle of the last generated object. The amount of increase is 
the golden ratio of a full rotation, which is approximately 222.5 degress (over 360).

## Possible applications

So, what can be used this for? Honestly, I
don't know what applications the pattern has, but it will only be useful when:

- Discrete points have to be organized with the same density, across time and
  space.
- The number of points increases, maintaining the uniform density.
- The topology of space is 2D, flat and unlimited while growing.

### Bad applications

There are better solutions when those conditions don't apply:

- If the points can be more spaced (or less spaced) in the borders than in the
  center, any golden spiral can be used. No need for the speed function.
  Sunflowers look to me in this category.
- Holes in a solid structure (wheel rims, speaker cover, bee honeycombs). If the holes don't
  move, it's easier to do a squared or hexagonal pattern.
- Non-planar topology (satellite orbits). Even if new satellites are added, and
  uniform density is needed, Earth's topology is finite and spherical, so the
  goal is to have increasing density. Look at
  https://bitbucket.org/jmmut/golden-tracer for experiments on uniformingly
  increasing density in a finite space.

## Running the program
### Requirements

- C++ compiler (`sudo apt-get install build-essential`)
- opengl (`sudo apt-get install libgl1-mesa-dev`)
- cmake (`sudo apt-get install cmake`)
- conan (download [here](https://www.conan.io/downloads), recommended:
  `sudo apt-get install python-pip; sudo pip install setuptools wheel conan`)

### Build

Linux:

```
git clone https://bitbucket.org/jmmut/goldenflower.git
cd goldenflower
mkdir build && cd build
conan install --build missing ..
cmake -G "Unix Makefiles" ..
make
./bin/goldenflower
```

Windows:

```
git clone https://bitbucket.org/jmmut/goldenflower.git
cd goldenflower
conan install --build missing
cmake -G "Visual Studio 14 Win64"
cmake --build . --config Release
./bin/goldenflower.exe
```

### Controls

If this documentation gets outdated, the complete controls are in src/Manager.cpp::onKeyChange:

- p, SPACE: pause or resume execution.
- Keypad_PLUS, PLUS, f: increase FPS.
- Keypad_MINUS, MINUS, g: decrease FPS.
- s: increase speed of the objects.
- x: decrease speed of the objects.
- a: increase appearance rate.
- z: decrease appearance rate.
- r: restart points.
- d: increase point size.
- c: decrease point size.

